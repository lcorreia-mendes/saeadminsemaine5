# Playbook  

```yaml
---
- name: Ajouter un groupe sur le serveur
  hosts: servers  
  become: yes     

  tasks:
    - name: Ajouter le groupe
      group:
        name: playbook
        state: present

    - name: Ajouter l'utilisateur
      user:
        name: $USER
        group: playbook
        state: present
```

## Explication ligne par ligne : 

### Ligne 1 : 
```yaml
---
```
Cette ligne indique le début d'un document YAML.
<br>

### Ligne 2 : 
```yaml
- name: Ajouter un groupe sur le serveur
```
Définit une tâche avec le nom "Ajouter un groupe sur le serveur".
<br>

### Ligne 3 : 
```yaml
hosts: servers
```
Spécifie les hôtes sur lesquels cette tâche sera exécutée. Dans cet exemple, la tâche sera exécutée sur les hôtes définis dans le groupe "servers".
<br>

### Ligne 4 : 
```yaml
become: yes  
```
Indique que la tâche doit être exécutée en tant qu'utilisateur disposant des privilèges de superutilisateur.
<br>

### Ligne 6 : 
```yaml
tasks:
```
Définit le début des tâches de cette tâche.
<br>

### Ligne 7 : 
```yaml
- name: Ajouter le groupe
```
Définit une tâche nommée "Ajouter le groupe".
<br>

### Ligne 8-9-10 : 
```yaml 
group:
    name: playbook
    state: present
```
Utilise le module group pour créer un groupe avec le nom "playbook". La valeur state: present indique que le groupe doit être présent (c'est-à-dire créé s'il n'existe pas).

### Ligne 12 : 
```yaml 
- name: Ajouter l'utilisateur
```
Définit une tâche nommée "Ajouter l'utilisateur ".

<br>

### Ligne 13-14-15-16 : 
```yaml 
user:
    name: $USER
    group: playbook
    state: present
```
Utilise le module user pour selectionner le user $USER (personne qui exécute le script). La valeur state: present indique que l'utilisateur doit être présent (c'est-à-dire créé s'il n'existe pas). L'utilisateur est ajouté au groupe playbook.

## Explication global de l'utilité du script :

Ce script permet donc à l’utilisateur qui exécute le script de créer le groupe playbook sur le serveur et de s'ajouter à celui-ci.


